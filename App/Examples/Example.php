<?php

namespace HttpClient\App\Examples;

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use HttpClient\App\Repositories\Delete\DeleteRepository;
use HttpClient\App\Repositories\Get\GetRepository;
use HttpClient\App\Repositories\Option\OptionRepository;
use HttpClient\App\Repositories\Patch\PatchRepository;
use HttpClient\App\Repositories\Post\PostRepository;
use HttpClient\App\Repositories\Put\PutRepository;


/**
 * function to fetch API response
 * @param string $type
 * @param string $apiUrl
 * @param string $header
 * @param string/mixed $content
 * @return response
 */
function fetchApiResponse($type, $apiUrl, $header, $content, $options = [])
{
	switch ($type) {
		case 'DELETE':
			$response = (new DeleteRepository())->delete($apiUrl, $header, $content, $options);
			return $response;

		case 'GET':
			$response = (new GetRepository())->get($apiUrl, $header, $content, $options);
			return $response;

		case 'OPTIONS':
			$response = (new OptionRepository())->option($apiUrl, $header, $content, $options);
			return $response;

		case 'PATCH':
			$response = (new PatchRepository())->patch($apiUrl, $header, $content, $options);
			return $response;

		case 'POST':
			$response = (new PostRepository())->post($apiUrl, $header, $content, $options);
			return $response;

		case 'PUT':
			$response = (new PutRepository())->put($apiUrl, $header, $content, $options);
			return $response;
		
		default:
			return 'Method not allowed';
			
	}

}

// https://jsonplaceholder.typicode.com/
// https://dzikrirobbi.medium.com/rest-api-with-php-get-post-put-delete-8365fe092618

// Options API
// $response = fetchApiResponse(
// 	'OPTIONS',
// 	'https://corednacom.corewebdna.com/assessment-endpoint.php',
// 	"Content-Type: application/x-www-form-urlencoded", 
// 	[],
// 	['raw' => true]
// );

// $token = $response['response'];
// var_dump($token);

// $data = [
// 	'name' => 'Krishna Vishwakarma',
// 	'email' =>  'krishna@optimalvirtualemployee.com',
// 	'url'   => 'https://bitbucket.org/krishnavove/http-client-coredna'
// ];

// $jsonData = json_encode($data);


// $headers = 'authorization: Bearer '. $token . "\r\n" . 'cache-control: no-cache' . "\r\n" . 'content-type: application/json';
// // POST API
// $response = fetchApiResponse(
// 	'POST',
// 	'https://corednacom.corewebdna.com/assessment-endpoint.php',
// 	$headers,
// 	$jsonData,
// 	['raw' => true]
// );

// var_dump($response);

// // GET API
// var_dump(fetchApiResponse(
// 	'GET',
// 	'https://jsonplaceholder.typicode.com/posts',
// 	"Content-Type: application/x-www-form-urlencoded", 
// 	''
// ));


// // POST API
// $response = fetchApiResponse(
// 	'POST',
// 	'https://www.stableservicedesk.faveodemo.com/v3/api/login',
// 	"Content-type: application/x-www-form-urlencoded; charset=UTF-8;\r\n"."Connection: close\r\n",
// 	"user_name=demo_admin&password=demopass"
// );


/*
// may be record does not exist, some time, try when record is there
// DELETE API
var_dump(fetchApiResponse(
	'DELETE',
	'http://dummy.restapiexample.com/api/v1/delete/1',
	"Content-type: application/x-www-form-urlencoded; charset=UTF-8;\r\n"."Connection: close\r\n",
	""
));
*/


/*
// PATCH API
$content = json_encode(['title' => 'foo']);
var_dump(fetchApiResponse(
	'PATCH',
	'https://jsonplaceholder.typicode.com/posts/1',
	"Content-type': 'application/json; charset=UTF-8;\r\n",
	$content
));
*/



/*
// PUT API
$content = json_encode(['id' => 1, 'title' => 'foo', 'body' => 'bar', 'userId' => 1]);
var_dump(fetchApiResponse(
	'PUT',
	'https://jsonplaceholder.typicode.com/posts/1',
	"Content-type': 'application/json; charset=UTF-8;\r\n"."Connection: close\r\n",
	$content
));
*/


?>
