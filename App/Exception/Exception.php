<?php

namespace HttpClient\App\Exception;

use Exception as PHPException;

/**
 * Exception for HTTP requests
 * @package HttpClient\App\Exception;
 */
class Exception extends PHPException {
	/**
	 * Type of exception
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * Create a new exception
	 *
	 * @param string $message Exception message
	 * @param string $type Exception type
	 * @param integer $code Exception numerical code, if applicable
	 */
	public function __construct($message, $type, $code = 0) {
		parent::__construct($message, $code);

		$this->type = $type;
		$this->data = $data;
	}

	/**
	 * Like {@see \Exception::getCode()}, but a string code.
	 *
	 * @codeCoverageIgnore
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

}
