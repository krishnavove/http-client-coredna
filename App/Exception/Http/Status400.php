<?php

namespace HttpClient\App\Exception\Http;

use HttpClient\App\Exception\Http;

/**
 * Exception for 400 Bad Request responses
 *
 * @package HttpClient\App\Exception\Http
 */
final class Status400 extends Http {
	/**
	 * HTTP status code
	 *
	 * @var integer
	 */
	protected $code = 400;

	/**
	 * Reason phrase
	 *
	 * @var string
	 */
	protected $reason = 'Bad Request';
}
