<?php

namespace HttpClient\App\Exception\Http;

use HttpClient\App\Exception\Http;

/**
 * Exception for 502 Bad Gateway responses
 *
 * @package HttpClient\App\Exception\Http;
 */
final class Status502 extends Http {
	/**
	 * HTTP status code
	 *
	 * @var integer
	 */
	protected $code = 502;

	/**
	 * Reason phrase
	 *
	 * @var string
	 */
	protected $reason = 'Bad Gateway';
}
