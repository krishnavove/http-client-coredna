<?php

namespace HttpClient\App\Exception\Http;

use HttpClient\App\Exception\Http;

/**
 * Exception for 412 Precondition Failed responses
 *
 * @package HttpClient\App\Exception\Http
 */
final class Status412 extends Http {
	/**
	 * HTTP status code
	 *
	 * @var integer
	 */
	protected $code = 412;

	/**
	 * Reason phrase
	 *
	 * @var string
	 */
	protected $reason = 'Precondition Failed';
}
