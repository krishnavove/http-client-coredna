<?php

namespace HttpClient\App\Exception\Http;

use HttpClient\App\Exception\Http;

/**
 * Exception for 416 Requested Range Not Satisfiable responses
 *
 * @package HttpClient\App\Exception\Http
 */
final class Status416 extends Http {
	/**
	 * HTTP status code
	 *
	 * @var integer
	 */
	protected $code = 416;

	/**
	 * Reason phrase
	 *
	 * @var string
	 */
	protected $reason = 'Requested Range Not Satisfiable';
}
