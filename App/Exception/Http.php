<?php

namespace HttpClient\App\Exception;

use HttpClient\App\Exception;
// use HttpClient\App\Exception\Http\StatusUnknown;

/**
 * Exception based on HTTP response
 *
 * @package HttpClient\App\Exception
 */
class Http extends Exception {
	/**
	 * HTTP status code
	 *
	 * @var integer
	 */
	protected $code = 0;

	/**
	 * Reason phrase
	 *
	 * @var string
	 */
	protected $reason = 'Unknown';

	/**
	 * Create a new exception
	 *
	 * There is no mechanism to pass in the status code, as this is set by the
	 * subclass used. Reason phrases can vary, however.
	 *
	 * @param string|null $reason Reason phrase
	 */
	public function __construct($reason = null) {
		if ($reason !== null) {
			$this->reason = $reason;
		}

		$message = sprintf('%d %s', $this->code, $this->reason);
		parent::__construct($message, 'httpresponse', $this->code);
	}

	/**
	 * Get the status message.
	 *
	 * @return string
	 */
	public function getReason() {
		return $this->reason;
	}

	/**
	 * Get the correct exception class for a given error code
	 *
	 * @param int|bool $code HTTP status code, or false if unavailable
	 * @return string Exception class name to use
	 */
	public static function get_class($code) {
		if (!$code) {
			throw new Exception('Unable to parse JSON data');
		}

		$class = sprintf('\HttpClient\App\Exception\Http\Status%d', $code);
		if (class_exists($class)) {
			return $class;
		}

	}
}
