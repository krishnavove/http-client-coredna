<?php

namespace HttpClient\App\Repositories\Base\Contracts;

/**
 * Interface BasRepositoryInterface
 * @package HttpClient\App\Repositories\Base\Contracts
 */
interface BaseRepositoryInterface
{

    /**
     * @param  string      $apiUrl
     * @param  array/mixed $context
     * @param  array       $options
     * @return $response
     */
    public function fetchResponse($apiUrl, $context, $options);

}
