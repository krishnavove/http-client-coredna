<?php

namespace HttpClient\App\Repositories\Base;

// use Exception
// use HttpClient\App\Exception\Exception as CustomException;
use HttpClient\App\Repositories\Base\Contracts\BaseRepositoryInterface;

/**
 * Class BaseRepository
 * @package HttpClient\App\Repositories
 */
class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @param  string      $apiUrl
     * @param  array/mixed $context
     * @param  array       $options
     * @return $response
     */
    public function fetchResponse($apiUrl, $context, $options)
    {
      
        $response    = @file_get_contents($apiUrl, false, $context);
        $data['header'] = $http_response_header;
        // $status_line = $data['header'][0];
        // preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);
        // $statusCode  = $match[1]; 
        // $error = error_get_last();

        // if (in_array($statusCode[0], [3, 4, 5])) {
        //     throw new Exception($error['message'], $statusCode);
        // }

        if ($response === false) {
            $error = error_get_last();
            return "HTTP request failed. Error was: " . $error['message'];
        } else if(isset($options['raw'])) {
            $data['response'] = $response;
            return $data;
        }else {
            $data['response'] = json_decode($response, true);

            if (JSON_ERROR_NONE !== json_last_error()) {
                return "Failed to parse json: " . json_last_error_msg();
            }
        }

        return $data;
    }

}
