<?php

namespace HttpClient\App\Repositories\Put\Contracts;

/**
 * Interface PutRepositoryInterface
 * @package HttpClient\App\Repositories\Put\Contracts
 */
interface PutRepositoryInterface
{

    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function put($apiUrl, $header, $content, $options = []);

}
