<?php

namespace HttpClient\App\Repositories\Put;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Put\Contracts\PutRepositoryInterface;

/**
 * Class PutRepository
 * @package HttpClient\App\Repositories
 */
class PutRepository extends BaseRepository  implements PutRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function put($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "PUT",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData, $options);

        return $response;
    }

}
