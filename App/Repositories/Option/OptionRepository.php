<?php

namespace HttpClient\App\Repositories\Option;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Option\Contracts\OptionRepositoryInterface;
// implements OptionRepositoryInterface

/**
 * Class OptionRepository
 * @package HttpClient\App\Repositories
 */
class OptionRepository extends BaseRepository implements OptionRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function option($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "OPTIONS",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData, $options);

        return $response;
    }

}
