<?php

namespace HttpClient\App\Repositories\Option\Contracts;

/**
 * Interface OptionRepositoryInterface
 * @package HttpClient\App\Repositories\Option\Contracts
 */
interface OptionRepositoryInterface
{

    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function option($apiUrl, $header, $content, $options = []);

}
