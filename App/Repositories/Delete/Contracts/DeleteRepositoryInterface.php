<?php

namespace HttpClient\App\Repositories\Delete\Contracts;

/**
 * Interface DeleteRepositoryInterface
 * @package HttpClient\App\Repositories\Delete\Contracts
 */
interface DeleteRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function delete($apiUrl, $header, $content, $options = []);

}
