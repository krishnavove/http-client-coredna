<?php

namespace HttpClient\App\Repositories\Delete;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Delete\Contracts\DeleteRepositoryInterface;

/**
 * Class DeleteRepository
 * @package HttpClient\App\Repositories\Delete
 */
class DeleteRepository extends BaseRepository implements DeleteRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function delete($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "DELETE",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData);

        return $response;
    }

}
