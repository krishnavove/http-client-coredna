<?php

namespace HttpClient\App\Repositories\Get\Contracts;

/**
 * Interface GetRepositoryInterface
 * @package HttpClient\App\Repositories\Get\Contracts
 */
interface GetRepositoryInterface
{

    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function get($apiUrl, $header, $content, $options = []);

}
