<?php

namespace HttpClient\App\Repositories\Get;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Get\Contracts\GetRepositoryInterface;

/**
 * Class GetRepository
 * @package HttpClient\App\Repositories
 */
class GetRepository extends BaseRepository implements GetRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function get($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "GET",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData, $options);

        return $response;
    }

}
