<?php

namespace HttpClient\App\Repositories\Patch\Contracts;

/**
 * Interface PatchRepositoryInterface
 * @package HttpClient\App\Repositories\Patch\Contracts
 */
interface PatchRepositoryInterface
{

    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function patch($apiUrl, $header, $content, $options = []);

}
