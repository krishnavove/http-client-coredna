<?php

namespace HttpClient\App\Repositories\Patch;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Patch\Contracts\PatchRepositoryInterface;

/**
 * Class BaseRepository
 * @package HttpClient\App\Repositories
 */
class PatchRepository extends BaseRepository  implements PatchRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function patch($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "PATCH",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData, $options);

        return $response;
    }
}
