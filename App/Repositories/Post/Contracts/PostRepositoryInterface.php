<?php

namespace HttpClient\App\Repositories\Post\Contracts;

/**
 * Interface PostRepositoryInterface
 * @package HttpClient\App\Repositories\Post\Contracts
 */
interface PostRepositoryInterface
{

    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options any thing extra
     * @return response
     */
    public function post($apiUrl, $header, $content, $options = []);

}
