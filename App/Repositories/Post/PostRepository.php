<?php

namespace HttpClient\App\Repositories\Post;

use HttpClient\App\Repositories\Base\BaseRepository;
use HttpClient\App\Repositories\Post\Contracts\PostRepositoryInterface;

/**
 * Class PostRepository
 * @package HttpClient\App\Repositories
 */
class PostRepository extends BaseRepository  implements PostRepositoryInterface
{
    /**
     * @param  string       $apiUrl
     * @param  string       $header
     * @param  string/JSON  $content
     * @param  array        $options // anything extra
     * @return response
     */
    public function post($apiUrl, $header, $content, $options = [])
    {
        $data = [
            "http" => [
                "method"            => "POST",
                "header"            => $header,
                "ignore_errors"     => true,
                "timeout"           => 30,
                "content"           => $content
            ],
            "ssl"  => [
                "allow_self_signed" => true,
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        ];

        $streamContextData = stream_context_create($data);

        $response          = $this->fetchResponse($apiUrl, $streamContextData, $options);

        return $response;
    }

}
